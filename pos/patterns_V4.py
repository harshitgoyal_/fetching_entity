#Subject
#{'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX', 'RIGHT_ATTRS' : {'DEP' : {"IN" : ['nmod', 'amod']}}}
_subject_patterns = [
  [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'aux'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'neg'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_C', 'RIGHT_ATTRS' : {'DEP' : 'xcomp'}},
  ], [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'aux'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'xcomp'}},
  ], [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'aux'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'neg'}},
    ], [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'amod'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'compound'}},
  ], [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'compound'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '$-', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'amod'}},
  ], [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : {'IN' : ['xcomp', 'amod']}}}
  ],  [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'SUBJECT', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'compound'}}
    ], [
    {'RIGHT_ID' : 'SUBJECT', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    ]
  ]

_predicate_patterns = [
  [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : {'IN' : ['appos', 'dep']}}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : {'IN' : ['appos', 'dep']}}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'prep'}},
    {'LEFT_ID' : 'SUFFIX_A', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A001', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'xcomp'}},
    {'LEFT_ID' : 'SUFFIX_A', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A001', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'conj'}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'compound'}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'appos'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'ROOT'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'xcomp'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
    {'LEFT_ID' : 'SUFFIX_A', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A01', 'RIGHT_ATTRS' : {'DEP' : 'amod'}},
    ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX', 'RIGHT_ATTRS' : {'DEP' : 'compound'}},
  ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'dep'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_A', 'RIGHT_ATTRS' : {'DEP' : 'nummod'}},
    ], [
    {'RIGHT_ID' : 'PREDICATE', 'RIGHT_ATTRS' : {'DEP' : 'nmod'}},
    {'LEFT_ID' : 'PREDICATE', 'REL_OP' : '>', 'RIGHT_ID' : 'SUFFIX_B', 'RIGHT_ATTRS' : {'DEP' : 'case'}},
  ]
  ]

