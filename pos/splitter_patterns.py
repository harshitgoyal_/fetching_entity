#_verb_patterns = [[{"POS" : {"IN" : ["PART", "AUX", "ADV", "VERB"]}, "OP" : "*"}]]
_action_verb_patterns = [
  [{'POS' :'ADJ', 'DEP' : 'amod'}, {'POS' : 'NOUN', 'DEP' : 'nmod'}],
  [{'POS' : 'VERB', 'DEP' : 'aux'}, {'POS' : 'PART', 'DEP' : 'neg'}, {'POS' : 'VERB', 'DEP' : 'conj'}],
  [{'POS' : 'VERB', 'DEP' : 'aux'}, {'POS' : 'PART', 'DEP' : 'neg'}, {'POS' : 'VERB', 'DEP' : 'ROOT'}],
  [{'POS' : 'VERB', 'DEP' : 'aux'}, {'POS' : 'VERB', 'DEP' : 'conj'}],
  [{'POS' : 'NOUN', 'TAG' : 'NN', 'DEP' : 'compound'}, {'POS' : 'VERB', 'TAG' : 'VBG', 'DEP' : 'nmod'}],
  [{'POS' : 'ADJ', 'DEP' : 'amod'}, {'POS' : 'NOUN', 'DEP' : 'compound'}],
  [{"POS" : 'NOUN', 'DEP' : 'compound'}, {"POS" : "VERB", "DEP" : "nmod", "OP" : "+"}],
  [{'POS' : 'NOUN', 'DEP' : 'nmod' }, {'POS' : 'ADP', 'DEP' : 'case'}, {'POS' : 'NUM', 'DEP' : 'nummod'}],
  [{'POS' : 'VERB', 'TAG' : 'VB', 'DEP' : 'xcomp'}, {'POS' : 'ADP', 'DEP' : 'case'}],

  #add
  [{'POS' : 'NOUN', 'TAG' : 'NN', 'DEP' : 'compound'}, {'POS' : 'PROPN', 'DEP' : 'compound'}],
  [{'POS' :'NOUN', 'TAG' : 'NN', 'DEP' : 'amod'}, {'POS' : 'NOUN', 'DEP' : 'compound'}],
  [{'POS' :'NOUN', 'TAG' : 'NN', 'DEP' : 'compound'}, {'POS' : 'NOUN', 'DEP' : 'compound'}],
  [{'POS' :'PROPN', 'TAG' : 'NNP', 'DEP' : 'compound'}, {'POS' : 'NOUN', 'TAG' : 'NNS', 'DEP' : 'dobj'}],
  [{'POS' :'VERB', 'TAG' : 'VBP', 'DEP' : 'aux'}, {'POS' : 'VERB', 'TAG' : 'VB', 'DEP' : 'advcl'}],
  [{'POS' :'VERB', 'TAG' : 'VB', 'DEP' : 'advcl'}, {'POS' : 'VERB', 'TAG' : 'VBG', 'DEP' : 'xcomp'}],
  # [{'POS' :'VERB', 'DEP' : 'aux'}, {'POS' : 'PART', 'TAG' : 'RB', 'DEP' : 'neg'}],
  [{'POS' :'NOUN', 'DEP' : 'appos'}, {'POS' : 'VERB', 'DEP' : 'xcomp'}],
  [{'POS' :'NOUN', 'DEP' : 'dep'}, {'POS' : 'VERB', 'DEP' : 'xcomp'}],
  [{'POS' :'ADV', 'TAG' : 'RB', 'DEP' : 'amod'}, {'POS' : 'PROPN', 'TAG' : 'NNP','DEP' : 'compound'}],
  [{'POS' :'PROPN', 'TAG' : 'NNP', 'DEP' : 'compound'}, {'POS' : 'NOUN', 'TAG' : 'NN', 'DEP' : 'ROOT'}],
   [{'ORTH': '7/7', 'DEP' : 'nummod', 'TAG' : 'CD'}, {'POS' : 'NOUN', 'DEP' : 'compound', 'ORTH': 'CABLE'}],
    [{'ORTH': 'OK', 'DEP': 'compound', 'TAG' : 'JJ'}, {'TAG' : 'RB', 'DEP' : 'amod', 'ORTH': 'EARLY'}],
    [{'DEP': 'compound', 'TAG' : 'NNP', 'POS': 'PROPN'}, {'TAG' : 'NN', 'DEP' : 'nmod', 'ORTH': 'start'}],
    [{'DEP': 'nummod', 'TAG' : 'CD', 'POS': 'NUM'}, {'TAG' : 'VBD', 'POS' : 'VERB', 'DEP': 'ROOT'}],
    [{'DEP': 'nummod', 'TAG' : 'CD', 'ORTH': '6AM'}, {'TAG' : 'VBD', 'DEP' : 'ROOT', 'ORTH': 'cut'}],
  ]

