# library
from flask import Flask, request
from CopyInstNotes import split_sentence
import spacy
from spacy.matcher import Matcher
from spacy.matcher import dependencymatcher
from spacy.matcher.dependencymatcher import *
from spacy.tokens import span
from spacy.util import filter_spans
import re
import pickle
import phonenumbers
import numpy as np
app = Flask(__name__)

'''sapcy is library for advanced natural language processing 
 It can be used to build information extraction or natural language understanding systems,
 or to pre-process text for deep learning. '''

@app.route("/")
def test_api():
  return "working"

# Pattern for fetching date's from text 
_date_patterns = [
  [{'POS': 'NUM', 'SHAPE': {'IN': ['dd/dd/dddd', 'dddd/dd/dd', 'dd/dd/dd', 'd/dd/dd', 'dd.dd.dddd', 'dddd.dd.dd', 'dd.dd.dd']}}],
  [{'POS': {'IN': ['NOUND', 'NUM']}, 'OP': '+'}, {'LOWER': {'IN': ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']}}, {'POS': 'NUM', 'op': '?'}],
  [{'LOWER': {'IN': ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']}}, {'POS': {'IN': ['NOUND', 'NUM']}, 'OP': '+'}, {'TEXT': ',', 'OP': '?'}, {'POS': 'NUM', 'op': '?'}],
  [{'LOWER': {'IN': ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']}}, {'POS': {'IN': ['NOUND', 'NUM']}, 'OP': '+'}, {'TEXT': ',', 'OP': '?'}, {'POS': 'NUM', 'op': '?'}],
  [{'ENT_TYPE': 'DATE'}],
  [{'POS': {'IN': ['NOUN', 'NUM']}, 'OP': '+'}, {'LOWER': {'IN': ['am', 'pm']}}, {'POS': 'NUM', 'op': '?'}],
   [{'POS': {'IN': ['NUM']}, 'OP': '+', 'POS': {'IN': ['NUM']}}],
  ]

# fetch the date from the text
def date_fetcher(text):
  processed_spans = []
  date = []
  raw_spans = []
  matcher = Matcher(nlp1.vocab)
  matcher.add('new_sent', _date_patterns)
  doc = nlp1(text)
  matches = matcher(doc)
  for m in matches:
    s = doc[m[1]: m[2]]
    raw_spans.append(s)
    processed_spans = filter_spans(raw_spans)
  for s in processed_spans:
    print(s.text+'--> date_fetcher')
    date.append(s.text)
  return date

# preprocess the text and break the text into sentences
def sentence_break(sentence):
  delimiters = "  ", "...", ". "
  sentence = sentence.replace("*", " ")
  regexPattern = '|'.join(map(re.escape, delimiters))
  list1 = re.split(regexPattern, sentence)
  while "" in list1:
      list1.remove("")
  return list1

#preprocesses the text for better result
def preprocessing(sentence):
    # sentence = sentence.replace("/",".")
    sentence = sentence.replace(".", "-")
    sentence = sentence.replace(" @ ", "at")
    sentence = sentence.lower()
    sentence = sentence.replace(" pm", "pm")
    sentence = sentence.replace(" am", "am")
    sentence = sentence.replace("cut off", "cutoff")
    return sentence

# fetching email from text
def email_fetcher(text):
  lst = re.findall('\S+@\S+', text)
  return lst

# fetching numbers from text
def Phone_fetcher(text):
  numbers = phonenumbers.PhoneNumberMatcher(text, "IN")
  num = []
  for number in numbers:
    num.append(number)
  return num


@app.route("/get_data", methods=['POST'])
def get_data():
  req_data = request.get_json() # request for data
  sentence = req_data['content']
  print(sentence)
  texts = sentence_break(sentence) # break the sentences and appnd into list
  phrases = []
  print("\n")
  for text in texts:
    splitter(text, phrases)
  doc1 = {}

  for index, text in enumerate(phrases): #repeat till all text will process
    print(text, "\n")
    vec_test = np.array(nlp1(text).vector)
    res = loaded_model.predict([vec_test])
    # rel_ent,rel_data = fetch_rel(text)
    # print(rel_ent, " ---> ", rel_data)
    if res[0] == 1:
      doc2 = {}
      sub = []
      doc2["text"] = text
      email = email_fetcher(text) #call email_fetcher function
      if email:
        doc2["email"] = email
        print(email, "-->", " email")
      number = Phone_fetcher(text)
      if number:
        doc2["number"] = number
        print(number, "-->", " num")
      doc = nlp(preprocessing(text)) # give entities of text
      for ent in doc.ents:
        if ent.label_ == 'SUB':
          sub.append(ent.text)
          doc2["subject"] = ent.text
        print(ent.text, "-->", ent.label_)
      doc2["subject"] = sub
      doc2["date"] = date_fetcher(text)
      doc1[str(index)] = doc2

  return doc1


def splitter(sentence, phrases):
  data = split_sentence(sentence) #import function from copyInstNotes
  if '' in data:
    data.remove('')
  if len(data) == 0:
    pass
  elif len(data) == 1:
    phrases.append(data[0])
    # print(data[0])
  else:
    for sent in data:
      splitter(sent, phrases)


if __name__ == "__main__":
    nlp = spacy.load("./models_airing_update") #extend en_core_web_lg model with our training data
    nlp1 = spacy.load('en_core_web_lg')        # spacy model for working with text
    loaded_model = pickle.load(open("C:/Users/harshit/Desktop/project/Sub_pred/fetching_entity/finalized_model1.pkl", 'rb'))
    app.run(debug=False, host='0.0.0.0', port=3000)
