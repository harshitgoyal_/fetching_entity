#!/usr/bin/env python
import logging
import spacy
from spacy.matcher import DependencyMatcher
from spacy.matcher import Matcher
from spacy.tokens import span
import re

import sys, os
path = os.getcwd()
import dateparser
from dateparser.search import search_dates

# importing function from py files
from splitter_patterns import _action_verb_patterns
from patterns_V4 import _subject_patterns, _predicate_patterns


_subject = 'SUBJECT'
_sec_subject = "SEC_SUBJ"
_predicate = 'PREDICATE'
_predicate_exclusions = ['ROOT', 'xcomp', 'punct']
_sent_splitters = [' and ', ' or ']
_nlp = spacy.load("en_core_web_sm")
# _nlp = en_core_sci_lg.load()
_token_exclusions = [':', ';', '--', '-', 'but', 'But', 'BUT']

_matcher = Matcher(_nlp.vocab)
_matcher.add('splitters1', _action_verb_patterns)

def split_sentence(mainText):
  listOfInstrution = []
  if mainText != '':
    listOfInstrution.append(mainText)

  '''Entry point'''
  #First initialize the matcher objects. 
  #We need two separate objects to handle the two fragments separately.

  dep_matcher= DependencyMatcher(_nlp.vocab)

  #Add the patterns to the objects.
  dep_matcher.add(_subject, _subject_patterns)
  dep_matcher.add(_predicate, _predicate_patterns)

  
  #Iterate through the sentences.
  for text in listOfInstrution:
    text = re.sub(' +', ' ', text)
    text = re.sub('\t+', ' ', text)
    # print(('\n{}: {}').format(i, text))
    text = text.strip(' ')
    texts = get_sentences(text)
  return texts

def get_sentences(text):
  '''Splits the specified text based on action verbs.'''
  split_points = []
  split_point = -1
  primary_doc = _nlp(text)
  length = len(primary_doc)
  sents = []

  for s in primary_doc.sents:
    sents.append(s.text)

  if len(sents) < 2:
    sents.clear()
    matches = _matcher(primary_doc)
    for m in matches:
      w = primary_doc[m[1]].text
      if not w[0].isdigit():
        split_points.append(m[1])

    if len(split_points) > 0:
      split_point = split_points[len(split_points) - 1]
      sents.append(primary_doc[0 : split_point].text.strip(' '))
      sents.append(primary_doc[split_point : length].text.strip(' '))
    else:
      sents.append(text)

  return sents
